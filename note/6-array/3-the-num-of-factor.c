/*************************************************************************
	> File Name: 3-the-num-of-factor.c
	> Author: 
	> Mail: 
	> Created Time: Wed 20 Mar 2024 09:55:24 AM CST
 ************************************************************************/

#include<stdio.h>
#define MAX_NUM 100

int arr[MAX_NUM + 5] = {0};
int prime[MAX_NUM + 5] = {0};
int fac_n[MAX_NUM + 5] = {0};
int power[MAX_NUM + 5] = {0};     //记录最小因子的次幂

void init(){
    for(int i = 2; i < MAX_NUM; i++){
        if(!arr[i]) {
            prime[++prime[0]] = i;
            fac_n[i] = 2;
            power[i] = 1;
        }
        for(int k = 1; k <= prime[0]; k++){
            if(prime[k] * i > MAX_NUM) break;
            arr[prime[k] * i] = 1;
            if(i % prime[k] == 0){
                fac_n[prime[k] * i] = fac_n[i]/(power[i] + 1) * (power[i] + 2) ;
                power[i * prime[k]] = power[i] + 1;
                break;
            }
            else{
                fac_n[prime[k] * i] = fac_n[prime[k]] * fac_n[i];
                power[i * prime[k]] = 1;
            }
        }
    }
}

int main(){
    init();

    for(int i = 0; i < MAX_NUM; i++){
        if(!arr[i])printf("arr[%d] = %d\n", i, arr[i]);
    }
    for(int k = 0; k < MAX_NUM; k++){
        if(prime[k])printf("prime[%d] = %d\n", k, prime[k]);      
    }
    for(int j = 0; j < MAX_NUM; j++){
        printf("factor_num[%d] = %d\n", j, fac_n[j]);
    }
}
