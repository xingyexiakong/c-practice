/*************************************************************************
	> File Name: 0.prime.c
	> Author: 
	> Mail: 
	> Created Time: Tue 19 Mar 2024 02:46:28 PM CST
 ************************************************************************/

#include<stdio.h>


int is_prime(int n){
    if(n < 2) return 1;
    for(int i = 2; i <= n/2; i++){
        if(n % i == 0) return 0;
    }
    return 1;
}

int main(){
    int n;
    while(~scanf("%d", &n)){
        printf("%d is %s\n", n, is_prime(n) ? "prime" : "not prime");
    }
}
