/*************************************************************************
	> File Name: 2-linear-sieve.c
	> Author: 
	> Mail: 
	> Created Time: Sun 17 Mar 2024 11:30:14 PM CST
 ************************************************************************/

#include<stdio.h>
#define MAX_NUM 100

int arr[MAX_NUM + 5] = {0};
int prime[MAX_NUM + 5] = {0};
void init(){

    for(int i = 2; i < MAX_NUM; i++){
        if(!arr[i]) prime[++prime[0]] = i;   //thinking: could we use #if(arr[i]) continue...   and why?
        for(int j = 1; j <= prime[j]; j++){
            if(i * prime[j] > MAX_NUM) break;
            arr[prime[j] * i] = 1;
            if(i % prime[j] == 0)break;
        }
    }

}

int main(){
    init();
    for(int i = 0; i < MAX_NUM; i++){
       !arr[i] && printf("arr[%d] = %d\n", i, arr[i]);
    }

    for(int i = 0; i < MAX_NUM; i++){
       prime[i] && printf("prime[%d] = %d\n", i, prime[i]);
    }
}
