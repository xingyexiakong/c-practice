/*************************************************************************
	> File Name: 1-prime-sieve.c
	> Author: 
	> Mail: 
	> Created Time: Sun 17 Mar 2024 11:21:02 PM CST
 ************************************************************************/

#include<stdio.h>
#define MAX_NUM 100

int arr[MAX_NUM + 5] = {0};

void init(){

    for(int i = 2; i < MAX_NUM; i++){
        if(arr[i]) continue;
        for(int j = 2 * i; j < MAX_NUM; j += i){
            arr[j] = 1;
        }
    }
}

int main(){
    init();
    for(int i = 0; i < MAX_NUM; i++){
        if(!arr[i])printf(" arr[%d] = %d \n", i, arr[i]);
    }
}
