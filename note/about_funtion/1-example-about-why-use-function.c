/*************************************************************************
	> File Name:mmZ1-example-about-why-use-function.c
	> Author: 
	> Mail: 
	> Created Time: Sun 03 Mar 2024 01:16:24 PM CST
    > content: input a number "n"(n > 0), and output the prime of the range from 1 to the number.
 ************************************************************************/

#include<stdio.h>
int isprime();
int main(){
    int n, flag;
    while(~scanf("%d", &n)){
        flag = 1;
        for(int i = 2 ; i < (n >> 1); i++){
            if(!(n % i)) flag = 0;
        }

        if (flag){
            printf("%d is prime!\n", n);
            printf("And output a %d\n", n * 2);
            
        }
        else{
            printf("%d is not prime\n", n);
            printf("And output a %d\n", n / 2);
        }
    }

}
/*
int isprime(int n){
    for(int i = 2; i <= (n >> 1); i++){
        if(!(n % i)) return 0;
    }
    return 1;
}

int func(int n){
    if (isprime(n)){
        return n * 2;
    }
    else return n / 2;
}
int main(){
    int n;
    while(~scanf("%d", &n)){
        if(isprime(n)){
            printf("%d is prime!\n", n);
        }
        else{
            printf("%d is not prime\n", n);
        }
        printf("And output a %d\n", func(n));
    }
    
}
*/

