/*************************************************************************
	> File Name: 10-my-print.c
	> Author: 
	> Mail: 
	> Created Time: Fri 15 Mar 2024 09:00:42 PM CST
 ************************************************************************/

#include<stdio.h>
#include<stdarg.h>
#include<stdlib.h>
#include<string.h>
#define swap(a, b){\
    a = a ^ b;\
    b = a ^ b;\
    a = a ^ b;\
}


void reverse_string(char *s, int n){
    int t = n/2 ;
    for(int i = 0; i < t; i++){
        swap(s[i], s[n - i - 1])
    }
}
char *turn_string(int n){
    if(!n){
        char *res = malloc(sizeof(char));
        res[0] = '0';
        return res;
    }
    int bit = 0, temp = n, sign = 0;
    if(n < 0){
        sign++;
        n = -n;
    }
    while(temp){
        temp /= 10;
        bit++;
    }
    

    char *res= (char *)malloc(sizeof(char) * (bit + sign));

    if(sign) res[0] = '-';
    int ind = sign;

    while(ind != bit + sign){
        char a = n % 10 + 48;
        n /= 10;
        res[ind] = a;
        ind++;
    }
    reverse_string(res + sign, bit);
    return res;
}    

int my_printf(const char *frm, ...){
    
    int cnt = 0, ind = 0;
    va_list arg;
    va_start(arg, frm);
    do{
        if(frm[ind] == '%'){
            switch(frm[ind + 1]) {
                case '%': {putchar('%');cnt++,ind++;}break;
                case 'd': {
                    int temp = va_arg(arg, int);
                    char *string = turn_string(temp);
                    cnt += my_printf(string);
                    ind++;
                }break;
            }
        }
        else{
            putchar(frm[ind]);
            cnt++;
        }

    }while(frm[ind++]);
    
    va_end(arg);
    return cnt - 1;
}

int main(){
 
   
   printf("os_printf:%d\n", printf("I am printing\n"));
   my_printf("my_printf:%d\n", my_printf("I am printing\n"));    
   printf("os_printf:%d\n", printf("%d %d\n",100, -100));
   my_printf("my_printf:%d\n", my_printf("%d %d\n",100, -100));    
   printf("os_printf:%d\n", printf("%d\n",-100));
   my_printf("my_printf:%d\n", my_printf("%d\n",-100));    
}












