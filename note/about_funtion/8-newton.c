/*************************************************************************
	> File Name: 8-newton.c
	> Author: 
	> Mail: 
	> Created Time: Sun 10 Mar 2024 08:31:45 PM CST
 ************************************************************************/

#include<stdio.h>
#include<math.h>
#define EPSL 1e-6

double y(double x, double n){
    return x * x - n;
}

double partial_y(double x){
    return 2 * x;
}

double newton(double (*F)(double, double), double (*f)(double),double n){

    double x = 1.0;
    while(fabs(F(x, n)) > EPSL){
        x -= F(x, n) / f(x);
    }
    return x;

}

int main(){

    double n;
    while(~scanf("%lf", &n)){
        printf("mysqrt(%lf) = %lf\n", n, newton(y, partial_y,n));
        printf("sqrt(%lf) = %lf\n", n, sqrt(n));

    }
}
