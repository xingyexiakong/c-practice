/*************************************************************************
	> File Name: 3-factorial.c
	> Author: 
	> Mail: 
	> Created Time: Sun 03 Mar 2024 02:28:28 PM CST
 ************************************************************************/

#include<stdio.h>

int factorial(int n){
    if(n == 1 || n == 0) return 1;
    return n * factorial(n - 1);
}

int main(){
    int n;
    while(~scanf("%d", &n)){
        printf("%d! = %d\n", n, factorial(n));
    }
}
