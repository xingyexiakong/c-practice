/*************************************************************************
	> File Name: 7-mysqrt.c
	> Author: 
	> Mail: 
	> Created Time: Sun 10 Mar 2024 07:26:22 PM CST
 ************************************************************************/

#include<stdio.h>
#include<math.h>
#define EPSL 1e-6

double my_sqrt(double n){
    
    double mid = n / 2.0, tail = n + 1.0, head = 0;
    while(tail - head > EPSL){
        if(mid * mid < n) head = mid;
        else tail = mid;
        mid = (head + tail) / 2.0;
    }
    return mid;
        
}

int main(){

    double n;
    while(~scanf("%lf", &n)){
        printf("my_sqrt(%lf) = %lf\n", n, my_sqrt(n));
        printf("sqrt(%lf) = %lf\n", n, sqrt(n));
    }   

}
