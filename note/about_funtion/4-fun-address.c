/*************************************************************************
	> File Name: 4-fun-address.c
	> Author: 
	> Mail: 
	> Created Time: Sun 03 Mar 2024 08:16:28 PM CST
 ************************************************************************/

#include<stdio.h>

int add(int a, int b){
    return a + b;
}

int substract(int a, int b){
    return a - b;
}

int calculate(int (*f1)(int, int), int (*f2)(int, int), int a, int b){
    if(a > b){
        return f1(a, b);
    }
    else if(a < b){
        return f2(a, b);
    }
    return a;
}
int main(){
    int a, b;
    while(~scanf("%d%d", &a, &b)){
        printf("the result :%d\n", calculate(substract,add,a, b));
    }
}
