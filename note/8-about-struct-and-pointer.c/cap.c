/*************************************************************************
	> File Name: cap.c
	> Author: 
	> Mail: 
	> Created Time: Fri 29 Mar 2024 01:41:17 PM CST
 ************************************************************************/

#include<stdio.h>

typedef struct stu_a{
    int age;
    char score;
    char gender;
}stu_a;


typedef struct stu_b{
    char score;
    int age;
    char gender;
}stu_b;

int main(){
    printf("sizeof(stu_a) = %lu\n", sizeof(stu_a));
    printf("sizeof(stu_b) = %lu", sizeof(stu_b));
}

