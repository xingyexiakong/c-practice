/*************************************************************************
	> File Name: verify_computer.c
	> Author: 
	> Mail: 
	> Created Time: Sun 31 Mar 2024 12:41:42 PM CST
 ************************************************************************/

#include<stdio.h>

//测试是否为小端机
void is_little(){
    int num = 123456789;
    printf("int[0] =  %d\n", ((char *)(&num))[0]);
    printf("int[1] =  %d\n", ((char *)(&num))[1]);
    printf("int[2] =  %d\n", ((char *)(&num))[2]);
    printf("int[3] =  %d\n", ((char *)(&num))[3]);
}

int main(){
    is_little();
}
