/*************************************************************************
	> File Name: 4.prime.c
	> Author: 
	> Mail: 
	> Created Time: Wed 17 Jan 2024 09:13:40 AM CST
 ************************************************************************/

#include<stdio.h>
#define MAX_N 100
int prime[MAX_N + 5] = {0};

void init(){
    for(int i = 2; i < MAX_N; i++){
        if(prime[i]) continue;
        for(int j = 2 * i; j < MAX_N; j += i){
            prime[j]  = 1;
        }
    }
}

int main(){
    init();
    int num;
    printf("press the number you want to quest:");
    while(~scanf("%d", &num)){
        if(prime[num]) printf("%d is not the prime\n", num);
        else printf("%d is the prime!\n", num);
    }
    return 0;
}
