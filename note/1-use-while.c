/*************************************************************************
	> File Name: 1-use-while.c
	> Author: 
	> Mail: 
	> Created Time: Sun 14 Jan 2024 04:48:07 PM CST
 ************************************************************************/

#include<stdio.h>


int main(){
    int i = 1;
    do{
        printf("%d ",i);
        if(i % 10 == 0){
            printf("\n");
        }
    }while(i++ < 100);
}
