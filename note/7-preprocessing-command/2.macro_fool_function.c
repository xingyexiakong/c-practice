/*************************************************************************
	> File Name: 2.macro_fool_function.c
	> Author: 
	> Mail: 
	> Created Time: Mon 25 Mar 2024 06:55:55 PM CST
 ************************************************************************/

#include<stdio.h>
#define MAX(a, b) ((a) > (b) ? a : b)
#define P(func) printf("%s = %d\n", #func, func);


int main(){

    int a = 7;

    P(MAX(2, 3));
    P(5 + MAX(2, 3));
    P(MAX(2 ,MAX(2, 3)));
    P(MAX(2, 3 > 4 ? 3 : 4));
    P(MAX(a++, 3));
}
