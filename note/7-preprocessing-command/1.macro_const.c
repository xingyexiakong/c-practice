/*************************************************************************
	> File Name: 1.macro_const.c
	> Author: 
	> Mail: 
	> Created Time: Mon 25 Mar 2024 06:52:43 PM CST
 ************************************************************************/

#include<stdio.h>

#define PI 3.1415926535

int main(){
    int r = 5;
    printf("[r] = %d [S] = %f", r, PI * r * r);
}
