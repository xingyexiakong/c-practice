/*************************************************************************
	> File Name: log.c
	> Author: 
	> Mail: 
	> Created Time: Sun 24 Mar 2024 02:45:44 PM CST
 ************************************************************************/

#include<stdio.h>

#define DEBUG

#ifdef DEBUG
#define log(frm, args...){\
    printf("[FILE]:%s [LINE]:%d [func]:%s [content]:", __FILE__, __LINE__,__func__);\
    printf(#frm, ##args);\
    printf("\n");\
}
#else
#define log(frm, args...)
#endif


__attribute__((constructor))
int add(int a, int b){
    
//    log(a);
    return a + b;
}

int main(){
    log(add(1,2));
}
