/*************************************************************************
	> File Name: 3.initial_macro.c
	> Author: 
	> Mail: 
	> Created Time: Mon 25 Mar 2024 07:21:20 PM CST
 ************************************************************************/

#include<stdio.h>

#define P(a) printf("[%s] = %s\n", #a, a);

int main(){

    P(__DATE__);
    P(__FILE__);
    printf("[__LINE__] = %d", __LINE__);
    P(__TIME__);
    P(__func__);
    P(__FUNC__);
    P(__PRETTY_FUNCTION__);


}

