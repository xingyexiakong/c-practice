/*************************************************************************
	> File Name: 4.generic_macro.c
	> Author: 
	> Mail: 
	> Created Time: Mon 25 Mar 2024 10:36:44 PM CST
 ************************************************************************/

#include<stdio.h>

#define TYPE(a) _Generic((a), \
        int : "%d\n",\
        double: "%lf\n", \
        char * : "%s\n"\
)


__attribute__((constructor))
void print_int(int a){
    printf(TYPE(a), a);
}



__attribute__((constructor))
void print_double(double a){
    printf(TYPE(a), a);
}

int main(){
    char str[] = "hello";
    printf(TYPE(str), str);
}
