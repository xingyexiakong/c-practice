/*************************************************************************
	> File Name: 1-pointer.c
	> Author: 
	> Mail: 
	> Created Time: Wed 03 Apr 2024 07:05:00 PM CST
 ************************************************************************/

#include<stdio.h>


int main(){
    
    int a[5];
    int b;
    char c;
    printf("&b = %p, sizeof(&b) = %lu\n", &b, sizeof(&b));
    printf("a[0] = %p\n", a);
    printf("a[1] = %p\n", a + 1);
    printf("a[2] = %p\n", a + 2);
    printf("a[3] = %p\n", a + 3);
    printf("a[4] = %p\n", a + 4);
    printf("%p", &c);
}
