/*************************************************************************
	> File Name: 3-function-pointer.c
	> Author: 
	> Mail: 
	> Created Time: Wed 03 Apr 2024 10:42:00 PM CST
 ************************************************************************/

#include<stdio.h>

int add(int a, int b){
    return a + b;
}


int main(){
    typedef int (*func)(int, int);
    func f = add;
    printf("%d", f(1, 1));
}
