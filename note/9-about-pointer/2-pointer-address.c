/*************************************************************************
	> File Name: 2-pointer-address.c
	> Author: 
	> Mail: 
	> Created Time: Wed 03 Apr 2024 07:39:13 PM CST
 ************************************************************************/

#include<stdio.h>

int main(){

    int a;
    int *p = &a;
    int arr[3][3];
    printf(" &a = %p\n p = %p\n &p = %p\n", &a, p, &p);
    printf(" arr[0][0] = %p, sizeof(arr[0][0]) = %lu\n", arr, sizeof(&arr));
    printf(" arr[1][0] = %p, sizeof(arr[1][0]) = %lu\n", arr + 3, sizeof(arr + 3));
    printf(" arr[1][0] = %p, sizeof(arr[2][0]) = %lu\n", &arr + 1, sizeof(arr + 3 + 3 + 1));
    
}
