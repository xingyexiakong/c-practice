/*************************************************************************
	> File Name: ip_union.c
	> Author: 
	> Mail: 
	> Created Time: Sun 31 Mar 2024 12:00:43 PM CST
 ************************************************************************/

#include<stdio.h>

union ip{
    struct{
        unsigned char a1;
        unsigned char a2;
        unsigned char a3;
        unsigned char a4;
    }n;
    int num;
};

int main(){
    char str[20];
    int arr[4];
    union ip test;
    while(~scanf("%s", str)){
        sscanf(str,"%d.%d.%d.%d",arr, arr + 1 , arr + 2, arr + 3);
        test.n.a1 = arr[0];
        test.n.a2 = arr[1];
        test.n.a3 = arr[2];
        test.n.a4 = arr[3];
/*
        test.n.a1 = arr[3];
        test.n.a2 = arr[2];
        test.n.a3 = arr[1];
        test.n.a4 = arr[0];*/
        printf("ip = %d.%d.%d.%d\n", arr[0], arr[1], arr[2], arr[3]);
        printf("num = %d\n",test.num);
        printf("---------------------------------------------\n");
    }
}
