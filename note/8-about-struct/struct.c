/*************************************************************************
	> File Name: struct.c
	> Author: 
	> Mail: 
	> Created Time: Tue 26 Mar 2024 09:05:48 AM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

typedef struct student{
    int grade;
    int age;
    char *name;
    char score;
}student;

student edit(int grade, int age, char* name, char score){
    student stu;
    stu.grade = grade;
    stu.age = age;
    stu.name = name;
    stu.score = score;
    return stu;
}

void search(student stu){
    printf("[stu.name] = %s\n", stu.name);
    printf("[stu.grade] = %d\n", stu.grade);
    printf("[stu.age] = %d\n", stu.age);
    printf("[stu.score] = %c\n", stu.score);
}

int main(){
   
    student nic;
    char nicname[] = "Nicholas";
    nic = edit(4, 13, nicname, 'A');
    search(nic);

}
