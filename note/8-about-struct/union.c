/*************************************************************************
	> File Name: union.c
	> Author: 
	> Mail: 
	> Created Time: Fri 29 Mar 2024 02:00:24 PM CST
 ************************************************************************/

#include<stdio.h>


union node{
    struct{
        unsigned char a1;
        unsigned char a2;
        unsigned char a3;
        unsigned char a4;
    }a;
    int num;
};

int main(){
    union node node1;
    //printf("sizeof(node1) = %lu", sizeof(node1));
    
    printf("(before)node1.num = %d\n", node1.num);
    node1.a.a1 = 'a';
    node1.a.a2 = 'b';
    node1.a.a3 = 'c';
    node1.a.a4 = 'd';

    printf("(before)a1 = %c a2 = %c a3 = %c a4 = %c\n", node1.a.a1,node1.a.a2,node1.a.a3,node1.a.a4);
    printf("(after)node1.num = %d\n", node1.num);
    node1.num = 1111111111;
    printf("(after)a1 = %c a2 = %c a3 = %c a4 = %c\n", node1.a.a1,node1.a.a2,node1.a.a3,node1.a.a4);
}
