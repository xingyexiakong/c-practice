/*************************************************************************
	> File Name: head2.c
	> Author: 
	> Mail: 
	> Created Time: Mon 08 Apr 2024 08:34:21 PM CST
 ************************************************************************/

 void funC(int n){
     if(!n) return ;
     printf("funC = %d\n", n);
     funA(n-1);
 }
