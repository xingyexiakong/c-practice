/*************************************************************************
	> File Name: head1.c
	> Author: 
	> Mail: 
	> Created Time: Mon 08 Apr 2024 08:32:43 PM CST
 ************************************************************************/
void funA(int n){
    if(!n) return ;
    printf("funA = %d\n", n);
    funB(n - 1);
}

void funB(int n){
    if(!n) return ;
    printf("funB = %d\n", n);
    funC(n - 1);
}
