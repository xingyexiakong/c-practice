/*************************************************************************
	> File Name: test.c
	> Author: 
	> Mail: 
	> Created Time: Sun 14 Jan 2024 08:47:01 PM CST
 ************************************************************************/

#include<stdio.h>
#define MAX_N 100

int array[MAX_N + 5] = {0};

void init(){
    
    for(int i = 2; i < MAX_N; i++){
        if(array[i]) continue;
        for(int k = 2 * i; k < MAX_N; k += i){
            array[k] = 1;
        }
    }
}

int main(){
    int n;
    init();
    while(~scanf("%d", &n)){
        if(array[n]) printf("%d is not the prime\n", n);
        else printf("%d is the prime\n", n);
    }
}
