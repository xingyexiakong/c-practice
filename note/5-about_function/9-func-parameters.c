/*************************************************************************
	> File Name: 9-func-parameters.c
	> Author: 
	> Mail: 
	> Created Time: Thu 14 Mar 2024 08:45:17 PM CST
 ************************************************************************/

#include<stdio.h>
#include<inttypes.h>
#include<stdarg.h>

#define P(func){\
    printf("%s = %d\n", #func, func);\
}

// how to get the parameters list                       ------va_list
// how to get the first parameter position after Alpha   ------va_start
// how to get the next parameter                        ------va_arg
// how to free the action getting parameters            ------va_end
int max_int(int n, ...){
    int ans = INT32_MIN;
    va_list arg;
    va_start(arg, n);
    while(n--){
        int temp = va_arg(arg, int);
        if(temp > ans) ans = temp;
    }
    va_end(arg);
    return ans;
}



int main(){
    P(max_int(3, 1, 2, 3));
    P(max_int(5, 1, 9, 6, 4, 20));
    P(max_int(5, 5, 15, 25, 35, 45, 65, 78, 99));
    P(max_int(4, -4, 6, 0, 2));
    P(max_int(3, -5, -2, 0));
}
