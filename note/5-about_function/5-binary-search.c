/*************************************************************************
	> File Name: 5-binary-search.c
	> Author: 
	> Mail: 
	> Created Time: Thu 07 Mar 2024 07:34:09 PM CST
 ************************************************************************/

#include<stdio.h>
#define SIZE 10

int binary_search(int *arr, int aim,int start, int len){
    if(len < start ){
        return 0;
    }
    int mid = (start + len) >> 1;
    if(*(arr + mid) == aim) return mid;


    else if(*(arr + mid) > aim) return binary_search(arr, aim, start, mid - 1);
    else if(*(arr + mid) < aim) return binary_search(arr, aim, mid + 1, len);
}

//000000000000111111111111找第一个1
int bin_search1(int *arr, int aim, int len){
    
    int head = 0, tail = len, mid = (head + tail) >> 1;
    while(head < tail){
       printf("head = %d, mid = %d, tail = %d\n", head, mid, tail);
        if(*(arr + mid) == aim) tail = mid;
        else if(*(arr + mid) < aim) head = mid + 1;
        mid = (head + tail ) >> 1;
    } 
    return arr[head] == aim? head:-1;
}
//递归写法
int b_search1(int *arr, int aim,int head, int tail){
    if(head >= tail){
        return arr[head] == aim ? head : -1;
    }
    int mid = (head + tail) >> 1;
    if(*(arr + mid) == aim) return b_search1(arr, aim, head, mid);
    else if(*(arr + mid) < aim) return b_search1(arr, aim, mid + 1, tail);
}


//111111111111000000000000找最后一个1

int bin_search2(int *arr, int aim, int len){
    
    int head = 0, tail = len, mid = (head + tail + 1) >> 1;
    while(head < tail){
       printf("head = %d, mid = %d, tail = %d\n", head, mid, tail);
        if(*(arr + mid) ==  aim) head = mid;
        else tail = mid - 1;

        mid = (head + tail + 1) >> 1;
    }
    return *(arr + head) == aim ? head:-1;
}
//递归写法
int b_search2(int *arr, int aim, int head, int tail){
    if(head >= tail){
        return *(arr + head) == aim ? head : -1;
    }
    int mid = (head + tail + 1) >> 1;
    if(*(arr + mid) == aim) return b_search2(arr, aim, mid, tail);
    else return b_search2(arr, aim, head, mid - 1);
}


int main(){
    int arr[SIZE + 5] = {0};
    int n, num;
    scanf("%d", &n);
    for(int i = 0;i < n; i++){
        scanf("%d", &arr[i]);
    }
    printf("the last 1 is located %d\n", b_search2(arr ,1, 0 ,n));
        
}
