/*************************************************************************
	> File Name: 6-gcd.c
	> Author: 
	> Mail: 
	> Created Time: Sat 09 Mar 2024 11:03:29 PM CST
 ************************************************************************/

#include<stdio.h>


int gcd(int a, int b){
    return b ? gcd(b, a % b) : a;
}

int lcm(int a, int b){
    return a * b / gcd(a , b);
}

int main(){
    int a, b;
    while(~scanf("%d %d", &a, &b)){
        printf("the greatest common divisor is %d and the lcm is %d about %d and %d \n", gcd(a, b),lcm(a, b), a , b);
    }
}
